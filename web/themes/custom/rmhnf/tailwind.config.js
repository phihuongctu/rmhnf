module.exports = {
  mode: 'jit',
  content: [
    './templates/**/*.html.twig',
    './templates/*.html.twig',
    './components/**/*.twig',
    './components/**/*.stories.json',
    './components/**/*.stories.yml'
  ]
}
